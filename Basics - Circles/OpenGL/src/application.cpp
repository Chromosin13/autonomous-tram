#include <iostream>
#include <cstdlib>
#include <ctime>

//#include <GL/glew.h>
//#include <GLFW/glfw3.h>
#include <GL/GL.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#include <math.h>
#define ASSERT(x) if (!(x)) __debugbreak();
#define GLCall(x) GLClearError();\
    x;\
    ASSERT(GLLogCall(#x, __FILE__, __LINE__))

static void GLClearError() {
    while (glGetError() != GL_NO_ERROR);
}

static bool GLLogCall(const char* function, const char* file, int line) {
    while (GLenum error = glGetError()) {
        std::cout
            << "[OpenGL Error] (" << error
            << ") : " << file
            << " | Line: " << line
            << std::endl;
        return false;
    }
    return true;
};

const int steps = 100;
const float PI = 3.141592653589793238463;

float white[3] = { 255,255,255 };
float blue[3] = { 0,106,137 };
float red[3] = { 226,30,30 };
float green[3] = { 134,215,106 };
float black[3] = { 50,50,50 };
float grey[3] = { 217,217,217 };
float darkgrey[3] = { 130,130,130 };
float orange[3] = { 245,210,15 };

//Freetype2 Library Initialization
FT_Library library;

//error = FT_Init_FreeType(&library);
//if (#error)
//{
//
//}



static void Circle(float xPos, float yPos, float radius, float radian, float rgb[]) {
    const float angle = PI * radian / steps;
    
    float prevXPos = xPos;
    float prevYpos = radius - yPos;

    for (int i = 0; i <= steps; i++)
    {
        float newXPos = radius * sin(angle * i);
        float newYPos = -radius * cos(angle * i);

        glBegin(GL_TRIANGLES);
        glColor3ub(rgb[0], rgb[1], rgb[2]);
        glVertex3f(prevXPos, prevYpos, 0.0f);
        glVertex3f(xPos, yPos, 0.0f);
        glVertex3f(newXPos, newYPos, 0.0f);
        glEnd();
        prevXPos = newXPos;
        prevYpos = newYPos;

    }
}

static void Needle(float rgbPoint[], float rgbCirc[]) {
    
    Circle(0.0f, 0.0f, 0.1f, 2.f, grey);
    glRotatef(30.0f, 0.0f, 0.0f, 1.0f);
    glColor3ub(rgbPoint[0], rgbPoint[1], rgbPoint[2]);
    glBegin(GL_POLYGON);
    glVertex2f(-0.01f, 0.0f);
    glVertex2f(0.01f, 0.0f);
    glVertex2f(0.01f, 0.2f);
    glVertex2f(0.0f, 0.25f);
    glVertex2f(-0.01f, 0.2f);
    glEnd();
    Circle(0.0f, 0.0f, 0.02f, 2.f, rgbCirc);
}

static void RecInd(float rgb[], float side) {
    glBegin(GL_POLYGON);
    glColor3ub(rgb[0], rgb[1], rgb[2]);
    glVertex2f(-1*side, -1*side);
    glVertex2f(side, -1*side);
    glVertex2f(side, side);
    glVertex2f(-1*side, side);
    glEnd();
}

int main(void)
{
    GLFWwindow* window;

    /* Initialize the library */
    if (!glfwInit())
        return -1;

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(1080, 1080, "Base Speed Gauge", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    /*Syncronize transition using V-Sync*/
    glfwSwapInterval(1);

    /* Initiate glewinit() */
    glewInit();

    const float radius = 1.0f;
    const float angle = PI * 2.0f / steps;

    const float xPos = 0.0f;
    const float yPos = 0.0f;

    float prevXPos = xPos;
    float prevYpos = radius - yPos;


    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        /* Render here */
        glClear(GL_COLOR_BUFFER_BIT);
        
        glBegin(GL_LINES);
        glColor3ub(254, 0, 0);
        glVertex2f(-1.0f, 0.0f);
        glVertex2f(1.0f, 0.0f);
        glEnd();

        glBegin(GL_LINES);
        glColor3ub(254, 0, 0);
        glVertex2f(-1.0f, 0.1f);
        glVertex2f(1.0f, 0.1f);
        glEnd();

        //Left Power Gauge
        glPushMatrix();
        glTranslatef(-0.5f, -0.2f, 0.0f);

        Circle(0.0f, 0.0f, 0.3f, 2.f, white);

        Circle(0.0f, 0.0f, 0.285f, 2.5f, black);
        Circle(0.0f, 0.0f, 0.275f, 2.f, green);

        glPushMatrix();
        //glTranslatef(0.0f, -0.5f, 0.0f);
        glRotatef(-90.0f, 0.0f, 0.0f, 1.0f);
        Circle(0.0f, 0.0f, 0.275f, 1.f, white);
        glPopMatrix();

        Needle(darkgrey, green);

        glPopMatrix();
        
        //Main Gauge
        glPushMatrix();
        glTranslatef(0.0f, 0.25f, 0.0f);

        glPushMatrix();
        glRotatef(240.0f, 0.0f, 0.0f, 1.0f);
        Circle(0.0f, 0.0f, 0.3f, 0.15f, white);
        glPopMatrix();

        glPushMatrix();
        glRotatef(210.0f, 0.0f, 0.0f, 1.0f);
        Circle(0.0f, 0.0f, 0.3f, 0.15f, white);
        glPopMatrix();

        glPushMatrix();
        glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
        Circle(0.0f, 0.0f, 0.3f, 0.15f, white);
        glPopMatrix();

        glPushMatrix();
        glRotatef(150.0f, 0.0f, 0.0f, 1.0f);
        Circle(0.0f, 0.0f, 0.3f, 0.15f, orange);
        glPopMatrix();

        glPushMatrix();
        glRotatef(120.0f, 0.0f, 0.0f, 1.0f);
        Circle(0.0f, 0.0f, 0.3f, 0.15f, white);
        glPopMatrix();
        
        glPushMatrix();
        glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
        Circle(0.0f, 0.0f, 0.3f, 0.15f, white);
        glPopMatrix();

        Circle(0.0f, 0.0f, 0.285f, 2.5f, black);
        Circle(0.0f, 0.0f, 0.275f, 2.f, red);
        glPushMatrix();
        glRotatef(-150.0f, 0.0f, 0.0f, 1.0f);
        Circle(0.0f, 0.0f, 0.275f, 1.f, blue);
        glPopMatrix();
        
        glPushMatrix();
        //glTranslatef(0.0f, -0.5f, 0.0f);
        glRotatef(-90.0f, 0.0f, 0.0f, 1.0f);
        Circle(0.0f, 0.0f, 0.275f, 1.f, white);
        glPopMatrix();
        
        glPushMatrix();
        //glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
        glTranslatef(-0.15f, -0.1f, 0.0f);
        Circle(0.0f, 0.0f, 0.05f, 2.f, red);
        glPopMatrix();

        glPushMatrix();
        //glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
        glTranslatef(0.0f, -0.15f, 0.0f);
        RecInd(grey, 0.01f);
        glPopMatrix();


        glPushMatrix();
        Needle(green, green);
        glPopMatrix();
        glPopMatrix();

        //Right Brake Gauge
        glPushMatrix();
        glTranslatef(0.5f, -0.2f, 0.0f);

        Circle(0.0f, 0.0f, 0.3f, 2.f, white);

        Circle(0.0f, 0.0f, 0.285f, 2.5f, black);
        Circle(0.0f, 0.0f, 0.275f, 2.f, red);

        glPushMatrix();
        //glTranslatef(0.0f, -0.5f, 0.0f);
        glRotatef(-90.0f, 0.0f, 0.0f, 1.0f);
        Circle(0.0f, 0.0f, 0.275f, 1.f, white);
        glPopMatrix();

        Needle(darkgrey, red);

        glPopMatrix();
        

        glLoadIdentity();

        glFlush();

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}